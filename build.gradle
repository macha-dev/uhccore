plugins {
	id "java-library"
	id "maven-publish"
	id "com.github.johnrengelman.shadow" version "7.1.2"
	id "xyz.jpenilla.run-paper" version "1.0.6"
}

group = "net.zerodind"
version = "1.20.1-SNAPSHOT"

repositories {
	mavenCentral()
	maven { // For Spigot API
		name "SpigotMC"
		url "https://hub.spigotmc.org/nexus/content/groups/public/"
	}
	maven { // For PaperLib
		name "PaperMC"
		url "https://papermc.io/repo/repository/maven-public/"
	}
	maven { // For UhcCore NMS
		name "UhcCore"
		url "https://gitlab.com/api/v4/groups/uhccore/-/packages/maven"
	}
	maven { // For VaultAPI
		name "JitPack"
		url "https://jitpack.io"
		content {
			includeGroup "com.github.MilkBowl"
		}
	}
	maven { // For WorldEdit
		name "EngineHub"
		url "https://maven.enginehub.org/repo/"
	}
	maven { // For ProtocolLib
		name "dmulloy2"
		url "https://repo.dmulloy2.net/repository/public/"
	}
	maven { // For AnvilGUI
		name "CodeMC"
		url "https://repo.codemc.io/repository/maven-snapshots/"
	}
	flatDir { // For BiomeMapping
		dirs "$rootDir/libs"
	}
}

dependencies {
	compileOnly "org.spigotmc:spigot-api:1.15.2-R0.1-SNAPSHOT"
	compileOnly "com.google.code.findbugs:jsr305:3.0.2"
	compileOnly "com.github.MilkBowl:VaultAPI:1.7"
	compileOnly "com.comphenix.protocol:ProtocolLib:4.7.0"
	implementation "net.wesjd:anvilgui:1.5.3-SNAPSHOT"
	implementation "io.papermc:paperlib:1.0.5"
	implementation project(":Support-WorldEdit-6")
	implementation project(":Support-WorldEdit-7")
	implementation ":BiomeMapping-1.3"
	implementation "net.zerodind:uhccore-nms:1.0.0"
	runtimeOnly "net.zerodind:uhccore-nms-1_18_R1:1.0.0"
	runtimeOnly "net.zerodind:uhccore-nms-1_18_R2:1.0.0"
}

java {
	// Target Java 8 for Minecraft 1.8 support
	toolchain.languageVersion = JavaLanguageVersion.of(8)

	withJavadocJar()
	withSourcesJar()
}

tasks.named("shadowJar") {
	classifier = ""
	relocate "io.papermc.lib", "com.gmail.val59000mc.paperlib"
	relocate "net.wesjd.anvilgui", "com.gmail.val59000mc.anvilgui"
	relocate "com.pieterdebot.biomemapping", "com.gmail.val59000mc.biomemapping"
}

tasks.named("assemble") {
	dependsOn tasks.named("shadowJar")
}

tasks.named("processResources") {
	filesMatching("plugin.yml") {
		expand(version: version)
	}
}

tasks.named("runServer") {
	minecraftVersion "1.18.2"
	javaLauncher = javaToolchains.launcherFor {
		languageVersion = JavaLanguageVersion.of(17)
	}
	jvmArgs += [ "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005" ]
}

// Only publish the shadowed JAR
components.java {
	[configurations.runtimeElements, configurations.apiElements].each {
		withVariantsFromConfiguration(it) { skip() }
	}
}

publishing {
	publications {
		mavenJava(MavenPublication) {
			from components.java
		}
	}

	repositories {
		maven {
			name "GitLab"
			url "https://gitlab.com/api/v4/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven"
			credentials(HttpHeaderCredentials) {
				name = "Job-Token"
				value = System.getenv("CI_JOB_TOKEN")
			}
			authentication {
				header(HttpHeaderAuthentication)
			}
		}
	}
}
